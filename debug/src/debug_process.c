#include "../h/debug_process.h"

struct tm *tm;

// init a process then do the debug task
int init(FILE *debug_file)
{
    // if file doesnt exist, open a new one
    debug_file = fopen("debug_log.txt", "a+");
    // return 0 because file will be open successfully
    return 0;   
}

int release(FILE *debug_file)
{
    fclose(debug_file);
    return 0;
}

// get the time value
int init_time(void)
{
    time_t t = time(NULL);
    tm = localtime(&t);
#if 0
    printf("now: %d-%02d-%02d %02d:%02d:%02d\n", 
		    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday,
		    tm->tm_hour, tm->tm_min, tm->tm_sec);
#endif
    return 0;
}

int DBG_PRINT(FILE *debug_file, char *file_name, char *fun_name, char *para)
{
    init_time();
    debug_file = fopen("debug_log.txt", "a+");
    fprintf(debug_file, "%d-%02d-%-2d in %s->%s:%s\n", 
		    tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday, 
		    file_name, fun_name, para);
    fclose(debug_file);
    return 0;
}

int DBG_PRINTF(FILE *debug_file, const char *file_path, const char *fun_name, char* para)
{
    debug_file = fopen("debug_log.txt", "a+");
    fprintf(debug_file, "%s in %s->%s:%s\n", __DATE__, file_path, fun_name, para);
    fclose(debug_file);
    /* we add BDG_2_DB here first */
    DB_INSERT("Debug_log.db");

    return 0;
}

#if 0
int main(){
    FILE *newfile;
    int result;

    result = init_time();
    if(result != 0)
    {
        printf("init time fail\n");
    }	    
    DBG_PRINT(newfile, "debug.c", "main function", NULL);

    result = init(newfile);
    if(result != 0)
    {
        printf("debug file create fail!\n");
    }

    result = init_time();
    if(result != 0)
    {
        printf("init timer fail!\n");
    }

    //result = DBG_PRINT(newfile, "debug.c", "main function", NULL);
    if(result != 0)
    {
        printf("debug output fail!\n");
    }


    release(newfile);
    return 0;
}
#endif
