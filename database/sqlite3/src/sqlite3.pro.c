#include "../h/sqlite3.h"

static char *createsql = 
    "CREATE TABLE Contact(ID INTEGER PRIMARY KEY, Name VARCHAR(10), PhoneNumber VARCHAR(10));";

static char *insertsql = "INSERT INTO Contact VALUES(NULL, 'Fred', '0123456');";

static char *querysql = "SELCET * FROM Contact;";

int DB_TEST(){
    int            rows, cols, i, j;
    sqlite3        *db;
    char           *errMsg = NULL;
    char           **result;

    if(sqlite3_open("example1.db", &db))
    {
        printf("database open failed\n");
        return 1;
    }

    // make a sqlite table
    sqlite3_exec(db, createsql, 0, 0, &errMsg);

    // insert a new data
    sqlite3_exec(db, insertsql, 0, 0, &errMsg);
    sqlite3_exec(db, insertsql, 0, 0, &errMsg);

    // get the current data from table
    printf("%lld\n", sqlite3_last_insert_rowid(db));

    // dump all data from table
    sqlite3_get_table(db, querysql, &result, &rows, &cols, &errMsg);

    /* print all data */
    for(i=0;i<rows;i++)
    {
        for(j=0;j<cols;j++)
        {
	    printf("%s\t", result[i*cols+j]);
	}
	printf("\n");
    }
    /* release table */
    sqlite3_free_table(result);
    /* close db */
    sqlite3_close(db);

    return 0;
}
