#include "../h/sqlite3.h"

static char *createsql = 
    "CREATE TABLE Debug(ID INTEGER PRIMARY KEY, DATE VARCHAR(20), FILE VARCHAR(50), FUN VARCHAR(20), PARA VARCHAR(50));";

static char *insertsql = "INSERT INTO Debug VALUES(NULL, '2021-05-03', 'function.c', 'fun1()', 'blank test');";

static char *querysql = "SELCET * FROM Debug;";

int DB_INSERT(char *db_name){
    int            rows, cols, i, j;
    sqlite3        *db;
    char           *errMsg = NULL;
    char           **result;

    if(sqlite3_open(db_name, &db))
    {
        printf("database open failed\n");
        return 1;
    }

    // make a sqlite table
    sqlite3_exec(db, createsql, 0, 0, &errMsg);

    // insert a new data
    sqlite3_exec(db, insertsql, 0, 0, &errMsg);

    // get the current data from table
    printf("%lld\n", sqlite3_last_insert_rowid(db));

    // dump all data from table
    sqlite3_get_table(db, querysql, &result, &rows, &cols, &errMsg);

    /* print all data */
    for(i=0;i<rows;i++)
    {
        for(j=0;j<cols;j++)
        {
	    printf("%s\t", result[i*cols+j]);
	}
	printf("\n");
    }
    /* release table */
    sqlite3_free_table(result);
    /* close db */
    sqlite3_close(db);

    return 0;
}
