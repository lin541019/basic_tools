#ifndef __VAL_LIST_H__
#define __VA_LIST_H__

#include <stdio.h>
#include <stdlib.h>
/* we include some macro such as va_start, va_arg, va_end */
#include <stdarg.h>
#include <stdbool.h>

#define MAX_ARRAY_DIM 8 // assign biggest array size is 8

typedef struct
{
    int *base;
    int dim;
    int *bound;
    int *constants;
}Array;

void InitArray(Array *a, int dim, ...);

void DestroyArray(Array *A);

bool Locate(Array *A, va_list ap, int *off);

void Value(Array *a, int *e, ...);

void Assign(Array *A, int e, ...);

#endif /* #define __VA_LIST_H__ */
