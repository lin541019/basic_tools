#ifndef __SQSTACK_H__
#define __SQSTACK_H__

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#define sqstack_initsize 20
#define sqstack_increase_size 20
#define OK 1
#define ERROR 0

typedef struct
{
    int *base;
    int *top;
    int sqstack_size;
}sqstack;

#endif /* #define __SQSTACK_H__ */
