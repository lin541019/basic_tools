run_misc: module/misc_dev/src/misc_dev.c
	gcc module/misc_dev/src/misc_dev.c -o misc_app
	./misc_app
run_macro: module/function_test/macro_test/container_of
	gcc module/function_test/macro_test/list_head.c -o test_run.o
	./test_run.o
run_chrdev: module/chrdev/src/chrdev_app.c
	gcc module/chrdev/src/chrdev_app.c -o chrdev_app
	./chrdev_app
run_bookstore: module/bookstore/src/use_this.cpp
	g++ module/bookstore/src/use_this.cpp -o bookstore
	./bookstore
run_pthread: module/socket_server/src/pthread_server.c
	gcc module/socket_server/src/pthread_server.c -o server -pthread
	gcc module/socket_server/src/pthread_client.c -o client -pthread
	./server & ./client 127.0.0.1
run_ping: module/socket_server/src/ping_socket.c
	gcc module/socket_server/src/ping_socket.c -o exec_server.o
	sudo ./exec_server.o 192.168.43.3
run_socket: module/socket_server/src/connect_server.c
	gcc module/socket_server/src/connect_server.c -o exec_server.o
	./exec_server.o
run_debug: module/function_test/debug_test/debug_test.c
	gcc module/function_test/debug_test/debug_test.c  debug/src/debug_process.c database/sqlite3/src/sqlite3.c -lsqlite3 -o test_run.o
	./test_run.o
clean_misc:
	rm misc_app
clean_pthreadserver: 
	rm server
	rm client
clean_debug:
	rm test_run.o
clean_bookstore:
	rm bookstore
clean_socket:
	rm exec_server.o
clean_debug_db:
	rm test_run.o
	rm Debug_log.db
	rm debug_log.txt
