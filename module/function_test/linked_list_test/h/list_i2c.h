#ifndef __LIST_I2C_H__
#define __LIST_I2C_H__

#include <string.h>
#include "list.h"

#define MAX_NAME_LEN 32
#define MAX_ID_LEN 10

struct list_head device_list;

#define I2C_TYPE 1
#define SPI_TYPE 2

char *dev_name[] = 
{
    "none",
    "I2C",
    "SPI"
};

struct device
{
    int type;
    char name[MAX_NAME_LEN];
    struct list_head list;
};

struct i2c_node
{
    int data;
    unsigned int reg;
    struct device dev;
};

struct spi_node
{
    unsigned int reg;
    struct device dev;
};

#endif /* #define __LIST_I2C_H__ */
