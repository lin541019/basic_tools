#ifndef __LIST_MIX_H__
#define __LIST_MIX_H__

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <byteswap.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <ctype.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include "list.h"

#undef NULL
#define NULL ((void *)0)

enum {
    false = 0,
    true = 1,
};

#define DATA_TYPE 0x14
#define SIG_TYPE 0x15

struct mylist{
    int number;
    char type;
    char *pmem;
    struct list_head list;
};

#define FATAL do {fprintf(stderr, "Error at line %d, file %s (%d)[%s]\n", __LINE__, __FILE__, errno, strerror(errno)); exit(1);}while(0)

struct list_head active_head;
struct list_head free_head;
#define BUFFER_NUM 10
#define DATA_BUFFER_SIZE 12
struct mylist * list_array[BUFFER_NUM];

#endif /* #define __LIST_MIX_H__ */
