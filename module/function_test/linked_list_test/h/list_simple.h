#ifndef __LIST_SIMPLE_H__
#define __LIST_SIMPLE_H__

#include <string.h>
#include "list.h"

#define MAX_NAME_LEN 32
#define MAX_ID_LEN 10

struct list_head myhead;

#define I2C_TYPE 1
#define SPI_TYPE 2

char *dev_name[] = 
{
    "none",
    "i2c",
    "SPI"
};

struct mylist
{
    int type;
    char name[MAX_NAME_LEN];
    struct list_head list;
};

#endif /* #define __LIST_SIMPLE_H__ */
