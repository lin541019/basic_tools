#include "../h/list_simple.h"

void display_list(struct list_head *list_head)
{
    int i = 0;
    struct list_head *p;
    struct mylist *entry;
    printf("---------list----------\n");
    list_for_each(p, list_head)
    {
        printf("node[%d]\n", i++);
        entry = list_entry(p, struct mylist, list);
        printf("\ttype: %s\n", dev_name[entry->type]);
        printf("\tname: %s\n", entry->name);
    }
    printf("--------end------------\n");
}

int main(void)
{
    struct mylist node1;
    struct mylist node2;
    
    INIT_LIST_HEAD(&myhead);
  
    node1.type = I2C_TYPE;
    strcpy(node2.name, "tzuminglinux");

    node2.type = I2C_TYPE;
    strcpy(node2.name, "tzuminglinux");

    list_add(&node1.list, &myhead);
    list_add(&node2.list, &myhead);

    display_list(&myhead);

    list_del(&node1.list);
    
    display_list(&myhead);
    return 0;
}

