/** brief : its the in-completed code, please update athe test completed version while finishing testing
*/


#include "../h/list_mix.h"

int born_number(int number)
{
    struct mylist *entry = NULL;
    if(list_empty(&free_head) == true)
    {
        printf("list free_head is empty!-----------------\n");
    }
    entry = list_first_entry(&free_head, struct mylist, list);
    entry->type = DATA_TYPE;
    entry->number = number;
    list_move_tail(&entry->list, &active_head);
}

int eat_node()
{
    struct mylist *entry = NULL;
    if(list_empty(&active_head) == true)
    {
        printf("list active_head is empty!----------------\n");
    }

    entry = list_first_entry(&active_head, struct mylist, list);
    printf("\t eat node=%d\n", entry->number);
    list_move_tail(&entry->list, &free_head);
}

void spit_node()
{
    struct mylist * entry = NULL;
    if(list_empty(&free_head) == true)
    {
        printf("list free_head is empty!------------------\n");
    }
    entry = list_first_entry(&free_head, struct mylist, list);
    printf("\t spit node=%d\n", entry->number);
    list_move_tail(&entry->list, &active_head);
}

void show_list(struct list_head *list_head)
{
    int i = 0;
    struct mylist *entry, *tmp;
    
    if(list_empty(list_head)==true)
    {
        return;
    }

    list_for_each_entry_safe(entry, tmp, list_head, list)
    {
        printf("[%d]=%d\t", i++, entry->number);
        if(i%4==0)
        {
            printf("\n");
        }
    }
}

int list_num(struct list_head *list_head)
{
    int i;
    struct mylist *entry, *tmp;

    list_for_each_entry_safe(entry, tmp, list_head, list)
    {
        i++;
    }
    return i;
}

static ssize_t buffer_ring_init()
{
    int i = 0;
    
    for(i=0;i<BUFFER_NUM;i++)
    {
        list_array[i] = malloc(sizeof(struct mylist));
        INIT_LIST_HEAD(&list_array[i]->list);
        list_array[i]->pmem = kzalloc(DATA_BUFFER_SIZE, GFP_KERNEL);
        list_add_tail(&list_array[i]->list, &free_head);
    }
    return 0;
}

static ssize_t insert_free_list_all()
{
    int i=0;
    for(i=0;i<BUFFER_NUM;i++)
    {
        list_add_tail(&list_array[i]->list, &free_head);
    }
    return 0;
}

static ssize_t buffer_ring_free()
{
    int buffer_count = 0;
    struct mylist * entry = NULL;
    for(;buffer_count<BUFFER_NUM;buffer_count++)
    {
        free(list_array[buffer_count]->pmem);
        free(list_array[buffer_count]);
    }
    return 0;
}

int main(int argc, char **argv)
{
    INIT_LIST_HEAD(&free_head);
    INIT_LIST_HEAD(&active_head);
    buffer_ring_init();
    insert_free_list_all();
    born_number(1);
    born_number(2);
    born_number(3);
    born_number(4);
    born_number(5);
    born_number(6);
    born_number(7);
    born_number(8);
    born_number(9);
    born_number(10);
   
    printf("\n-------------active list[%d]-------------\n", list_num(&active_head));
    show_list(&active_head);
    printf("\n-----------------end---------------------\n");

    printf("\n---------------free list[%d]-------------\n", list_num(&free_head));
    show_list(&free_head);
    printf("\n-----------------end---------------------\n");

    printf("\n\n          active list--------------> free list\n");
    eat_node();
    eat_node();
    eat_node();

    printf("\n------------active list[%d]--------------\n", list_num(&active_head));
    show_list(&active_head);
    printf("\n-----------------end---------------------\n");

    printf("\n-------------free list[%d]---------------\n", list_num(&free_head));
    show_list(&free_head);
    printf("\n-----------------end---------------------\n");

    printf("\n\n           free list--------------> active list\n");
    spit_node();
    spit_node();

    printf("\n------------active list[%d]--------------\n", list_num(&active_head));
    show_list(&active_head);
    printf("\n-----------------end---------------------\n");

    printf("\n-------------free list[%d]---------------\n", list_num(&free_head));
    show_list(&free_head);
    printf("\n-----------------end---------------------\n");
    return 0;
}

