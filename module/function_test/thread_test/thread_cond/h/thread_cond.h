#ifndef __THREAD_COND_H__
#define __THREAD_COND_H__

#include <stdio.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

pthread_t thread;
pthread_cond_t cond;
pthread_mutex_t mutex;
int flag = 1;

void * thr_fn(void * arg);



#endif /* #define __THREAD_COND_H__ */
