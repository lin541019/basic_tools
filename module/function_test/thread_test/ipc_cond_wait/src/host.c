#include "../h/common_lib.h"

int main()
{
    char output[] = "test";
    int shmid = shmget(IPC_PRIVATE, sizeof(output), 0666);
    FILE *fp;
    fp = fopen("/tmp/shmid.txt", "w+");
    fprintf(fp, "%d", shmid);
    fclose(fp);
    int *mem_arr;

    while(1)
    {
        mem_arr = (int*)shmat(shmid, NULL, 0);
        memcpy(mem_arr, output, sizeof(output));
        shmdt(mem_arr);
    }

    return 1;
}
