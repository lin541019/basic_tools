#include "../h/common_lib.h"

int main()
{
    char output[] = "test";
    FILE *fp;
    fp = fopen("/tmp/shmid.txt", "r");
    int shmid;
    fscanf(fp, "%d", &shmid);
    fprintf(fp, "%d", shmid);
    fclose(fp);
    
    while(1)
    {
        int* shared_mem = (int *)shmat(shmid, NULL, 0);
        printf("data: %d\n", shared_mem);
    }
    return 1;
}
