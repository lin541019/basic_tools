/** @file      : debug_test.c
  * @brief     : debug tools test
  * @details   : debug tools implement test
  * @author    : lzm
  * @version   : v1.0
  * @copyright : Copyright By lintzuming, All Rights Reserved 
  */

#include "../../../include/h/common_lib.h"
#include "../../../debug/h/debug_process.h"

FILE *debug_file;

int fun1()
{
    DBG_PRINTF(debug_file, __FILE__, __func__, "blank test");
}

int fun2()
{
    DBG_PRINTF(debug_file, __FILE__, __func__, "blank test");
}

int main()
{
    fun1();
    //fun2();
    return 0;
}
