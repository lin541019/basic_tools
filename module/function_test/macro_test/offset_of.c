#include "../../../include/h/offset_of.h"

int main()
{
    puts("--------offsetof-----\n");
    printf("---- next = %d -----\n", offsetof(struct MY_DATA, next));
    printf("---- Data_A = %d ----\n", offsetof(struct MY_DATA, Data_A));
    printf("---- Data_B = %d ----\n", offsetof(struct MY_DATA, Data_C));
    printf("---- Data_C = %d ----\n", offsetof(struct MY_DATA, Data_C));
    
    puts("\n---- my_offsetof----");
    printf("---- next = %d----\n", my_offsetof(struct MY_DATA, next));
    return 0;
} 
 
