#include "../../../include/h/container_of.h"

struct MY_DATA {
    int Data_A, Data_B, Data_C;
    struct MY_DATA *next;
};

int main()
{
    struct MY_DATA Obj_one;
    struct MY_DATA *RET;

    Obj_one.Data_A = 123;
    Obj_one.Data_B = 456;
    Obj_one.Data_C = 789;

    int *Data_B_ptr = &Obj_one.Data_B;
    RET = container_of(Data_B_ptr, struct MY_DATA, Data_B);

    puts("\n-------------- index number = Data_B----------");
    printf("Obj_one's addr = %p\nRET addr = %p\n", &Obj_one, RET);
    printf("RET->Data_A = %d\nRET->Data_B = %d\nRET->Data_C = %d\n", RET->Data_A, RET->Data_B, RET->Data_C);

    return 0;

}
