#include "../../../include/h/va_list.h"

void InitArray(Array *A, int dim, ...)
{
    if(dim < 1 || dim > MAX_ARRAY_DIM)
        return ;
    A->dim = dim;
    A->bound = (int *)malloc(dim * sizeof(int));

    int elemtotal = 1;
    va_list ap;
    va_start(ap, dim);
    for(int i=0;i<dim;++i)
    {
        A->bound[i] = va_arg(ap, int);
        elemtotal *= A->bound[i];
    }
    va_end(ap);

    A->base = (int *)malloc(sizeof(int) * elemtotal);
    A->constants = (int *)malloc(dim * sizeof(int));
    A->constants[dim-1] = 1;

    for(int i=dim-2;i>=0;--i)
    {
        A->constants[i] = A->bound[i+1] * A->constants[i+1];
    }
}

void DestroyArray(Array *A)
{
    if(!A->base)
        return;
    free(A->base);
    A->base = NULL;
    if(!A->bound)
    {
        return ;
    }
    free(A->bound);
    A->bound = NULL;

    if(!A->constants)
    {
        return;

    }
    free(A->constants);
    A->constants = NULL;
}

bool Locate(Array *A, va_list ap, int *off)
{
    *off = 0;
    for(int i=0;i< A->dim; ++i)
    {
        int ind = va_arg(ap, int);
        if(ind < 0 || ind > A->bound[i])
            return false;
        (*off) += A->constants[i] * ind;
    }

    return true;
}

void Value(Array *A, int *e, ...)
{
    va_list ap;
    va_start(ap, e);
    int off;
    if(!Locate(A, ap, &off))
    {
        return;
    }
    *e = *(A->base + off);
}

void Assign(Array *A, int e, ...)
{
    va_list ap;
    va_start(ap, e);
    int off;
    if(!Locate(A, ap, &off))
    {
        return;
    }
    *(A->base + off) = e;
}

int main(void)
{
    Array arr;
    InitArray(&arr, 3, 3, 4, 5);
    for(int i=0;i<3;++i)
    {
        for(int j=0;j<4;++j)
        {
            for(int k=0;k<5;++k)
            {
                Assign(&arr, i+j+k, i, j, k);
            }
        }
    }

    for(int i=0;i<3;++i)
    {
        for(int j=0;j<4;j++)
        {
            for(int k=0;k<5;++k)
            {
                int tmp = 1;
                Value(&arr, &tmp, i, j, k);
                printf("%5d", tmp);
            }
        
            printf("\n");
        }
        printf("\n");
    }
    printf("\n");

    return 0;
}
