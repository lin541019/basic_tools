#include "../../../include/h/sqstack.h"

int Create_sqstack(sqstack *s)
{
    s->base = (int *)malloc(sizeof(int) * sqstack_initsize);
    if(!(s->base))
        exit(0);
    s->top = s->base;
    
    s->sqstack_size = sqstack_initsize;
    return OK;
}

int Push(sqstack *s, int item)
{
    if(s->top - s->base >= s->sqstack_size)
    {
        printf("space not enough, need to increase space!\n");
        s->base = (int *)realloc(s->base, (s->sqstack_size + sqstack_increase_size) * sizeof(int));
        if(!(s->base))
             exit(0);
        s->top = s->base + s->sqstack_size;
        //*(s->top) = item;
        s->sqstack_size = s->sqstack_size + sqstack_increase_size;
    }

    *(s->top) = item;
    s->top++;
    return OK;
}

int Len_sqstack(sqstack *s)
{
    return s->top - s->base;
}

int pop(sqstack *s)
{
     while(s->top > s->base)
     {
         printf("%d\n", *(--s->top));
         // s->top--;
     }
     return OK;
}

int Trease_sqstack(sqstack *s)
{
     int *temp = s->base;
     while(temp < s->top)
     {
         printf("%d\n", *temp);
         temp++;
     }
     return OK;
}

int main()
{
     sqstack s;
     Create_sqstack(&s);
    
    for(int i=0;i<81;++i)
    {
        // Push(&s, 1);
        // Push(&s, 2);
        // Push(&s, 3);
        // Push(&s, 4);
        Push(&s, i);
    }
    
    Trease_sqstack(&s);
    //pop(&s);
    // Trease_sqstack(&s);
    return 0;
}
