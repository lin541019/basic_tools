/** @brief         chrdev_app.c
  * @param         to read / write data through char device kernel module
  * @retval        error_none
  * @author        lzm
  */
#include "../h/chrdev_app.h"

int main(void)
{
    printf("chrdev app test\n");

    // open the file
    int fd = open("/dev/chrdev1", O_RDWR);
    if(fd == -1)
    {
        printf("Error in opening file \n");
        exit(-1);
    }
    // write data
    write(fd, wbuf, strlen(wbuf));
    // read file content
    read(fd, rbuf, 128);
    // print read content
    printf("The content : %s\n", rbuf);
    // read complete, close file
    close(fd);
    return 0;
}
