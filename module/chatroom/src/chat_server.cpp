#include "../h/chat_local.h"
#include "../h/chat_utils.h"

using namespace std;

// save client socket description lsit
list<int> clients_list;

int handle_message(int client)
{
    char buf[BUF_SIZE], message[BUF_SIZE];
    bzero(buf, BUF_SIZE);
    bzero(message, BUF_SIZE);

    int len;

    CHK2(len, recv(client, buf, BUF_SIZE, 0));

    if(len == 0)
    {
        CHK(close(client));
        clients_list.remove(client);
    }
    else
    {
        if(clients_list.size() == 1)
        {
            CHK(send(client, STR_NOONE_CONNECTED, strlen(STR_NOONE_CONNECTED), 0));
            return len;
        }

        sprintf(message, STR_MESSAGE, client, buf);
        list<int>::iterator it;

        for(it = clients_list.begin(); it != clients_list.end();it++)
        {
            if(*it != client)
            {
                CHK(send(*it, message, BUF_SIZE, 0));
            }
        }
    }
    return len;
}

int main(int argc, char* argv[])
{
    int listener;
    struct sockaddr_in addr, their_addr;
    addr.sin_family =PF_INET;
    addr.sin_port = htons(SERVER_PORT);
    addr.sin_addr.s_addr = inet_addr(SERVER_HOST);
    socklen_t socklen;
    socklen = sizeof(struct sockaddr_in);

    static struct epoll_event ev, events[EPOLL_SIZE];
    ev.events = EPOLLIN | EPOLLET;

    char messages[BUF_SIZE];
    
    int epfd;            // epoLL description
    clock_t tStart;      // compute program execute time
    
    int client, res, epoll_events_count;
    
    CHK2(listener, socket(PF_INET, SOCK_STREAM, 0)); 
    setnonblocking(listener);                                   // set listen socket to nonblocking
    CHK(bind(listener, (struct sockaddr*)&addr, sizeof(addr))); // set listen socket
    CHK(listen(listener, 1));                                   // set listen
    CHK2(epfd, epoll_create(EPOLL_SIZE));                       // create a epoll description, add epoll to listen socket
    ev.data.fd = listener;
    CHK(epoll_ctl(epfd, EPOLL_CTL_ADD, listener, &ev));
    
    while(1)
    {
        CHK2(epoll_events_count, epoll_wait(epfd, events, EPOLL_SIZE, EPOLL_RUN_TIMEOUT));
        tStart = clock();
        for(int i=0;i<epoll_events_count;i++)
        {
            if(events[i].data.fd == listener)                   // new connection comes, put the link to epoll, then send the welcome message
            {
                CHK2(client, accept(listener, (struct sockaddr *)&their_addr, &socklen));
                setnonblocking(client);
                ev.data.fd = client;
                CHK(epoll_ctl(epfd, EPOLL_CTL_ADD, client, &ev));

                clients_list.push_back(client);                 // add new client to list
                bzero(messages, BUF_SIZE);
                res = sprintf(messages, STR_WELCOME, client);
                CHK2(res, send(client, messages, BUF_SIZE, 0));
            }
            else
            {   // notice : theres no qpoll_ctl reset socket event type, but still can recieve the message send from client
                CHK2(res, handle_message(events[i].data.fd));
            }
        }
        printf("Statistics : %d events handled at : %.2f second(s)\n", epoll_events_count, (double)(clock() - tStart / CLOCKS_PER_SEC));
    }
    close(listener);
    close(epfd);

    return 0;
}
