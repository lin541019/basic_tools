#ifndef __CHAT_UTILS_H__
#define __CHAT_UTILS_H__

#include "chat_local.h"

int setnonblocking(int sockfd)
{
    CHK(fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFD, 0) | O_NONBLOCK));
    return 0;
}

#endif // #define __CHAT_UTILS_H__
