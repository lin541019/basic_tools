#ifndef __CHAT_LOCAL_H__
#define __CHAT_LOCAL_H__

#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <list>
#include <string.h>

#define BUF_SIZE 1024                   // default buffer
#define SERVER_PORT 8888                // listen port
#define SERVER_HOST "127.0.0.1"     // server ip address
#define EPOLL_RUN_TIMEOUT -1            // epoll timeout
#define EPOLL_SIZE 10000                // max epoll listen clients

#define STR_WELCOME "Welcome to seChat! Your ID is : Client #%d"
#define STR_MESSAGE "Client #%d>> %s"
#define STR_NOONE_CONNECTED "Noone connected to server except you!"
#define CMD_EXIT "EXIT"

// two useful macro : check and do the test
#define CHK(eval) if(eval<0){perror("eval"); exit(-1);}
#define CHK2(res, eval) if((res = eval) < 0){perror("eval"); exit(-1);}

/** function name : setnonblocking
  * descrption    : set socket to nonblocking
  * input         : [in] sockfd socket file description
  * output        : none
  * return        : 0
  */
int setnonblocking(int sockfd);

/** function name : handle_message
  * descrption    : handle every client socket
  * input         : [in] new_fd socket description
  * output        : none
  * return        : 0
  */
int handle_message(int new_fd);

#endif // #define __CHAT_LOCAL_H__
