#ifndef __SHARE_MEMORY_H__
#define __SHARE_MEMORY_H__

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <string.h>
#include <unistd.h>

void* create_shared_memory(size_t size);

#endif
