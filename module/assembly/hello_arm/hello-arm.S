    .syntax unified
    .thumb

#include <sys/syscall.h>

    .section .text
    .global _start
    .thumb_func
_start:
    /* write(1, "hello_world", 13) */
    movs r7, SYS_write
    movs r0, 1
    ldr r1, =hello_str
    movs r2, hello_str_len
    svc 0

    /* exit_group(0) */
    movs r7, SYS_exit_group
    movs r0, 0
    svc 0

    .section .rodata
hello_str:
    ascii "hello, world\n"
    .set hello_str_len, . - hello_str
