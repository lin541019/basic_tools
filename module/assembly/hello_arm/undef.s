    area reset, code
    code32
    entry
start
    bl stack_init
stack_init
;   @undefine_stack    
    msr cpsr_c, #0xdb
    ldr sp, =0x34000000
    
;   @abort_stack
    msr cpsr_c, #0xd7
    ldr sp, =0x33f00000
;   @irq_stack
    msr cpsr_c, #0xd2
    ldr sp, =0x33e00000
;   @ sys_stack
    msr cpsr_c, #0xdf
    ldr sp, =0x33d00000
    msr cpsr_c, #0xd3
    mov pc. lr
end
