.data
hello_str:
    .ascii "Hello World\n"
hello_len = . - hello_str

.text
.global _start
_start:
    /* %r0 = write(1, hello_str, hello_len); */
    mov %r0, $1
    ldr %r1, =hello_str
    ldr %r2, =hello_len
    mov %r7, $4
    svc $0
    /* exit(%%r0) */
    mov %r7, $1
    svc $0

.end
