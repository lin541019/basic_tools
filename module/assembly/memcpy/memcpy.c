#include <stdio.h>

#if 0
void my_strcpy(char *src, char *dest)
{
    char ch;
    __asm__
    (
	"loop:;"
		"LDRB %2, %0, #1\n\t"
		"STRB %2, %1, #1\n\t"
		"CMP %2, #0\n\t"
		"BNE loop;"
		: "=r"(src)
		: "r"(dest), "r"(ch)
		: "cc"
    );
}

extern void arm_strcpy(char *str, char *dst);
#endif

#if 0
void asm_float(void)
{
    double j = 0.001;
    short int k = 10;
    double l;

    asm volatile
    (
	"vmov s8, %[k]\n\t"
	"vcnt.f64.s32 d6, s8\n\t"
	"vmul.f64 %P[l], %P[j], d6\n\t"
	: [l] "=w" (l)
	: [j] "w" (j), [k] "r" (k)
	: "d8", "s8"
    );

    printf("l = %f\n", l);
}
#endif

void asm_mem_to_mem(void)
{
    unsigned int var_1 = 0;
    unsigned int var_2 = 999;
    unsigned int var_3 = 0;

    __asm__ __volatile__
    (
        "mov %0, #20;"
	"mov %2, %1;"
	: "=r" (var_1), "=r"(var_3)
	: "r" (var_2)
	: "memory"
    );

    printf("var_1 : %d\nvar_2 : %d\nvar_3 : %d\n", var_1, var_2, var_3);
}

void asm_cpsr_to_mem(void)
{
    int cpsr_status = 0;
    asm
    (
        "mrs %0, cpsr;"
        :"=r" (cpsr_status)	 // declare output
	:
	: "memory"
    );
    printf("cpsr : %d\n", cpsr_status);
}

int main()
{
#if 0
    char *a = "hello world";
    char b[64];
    arm_strcpy(a, b);
    printf("a : %s\n", a);
    printf("b : %s\n", b);
#endif
    asm_mem_to_mem();
    asm_cpsr_to_mem();
    return 0;
}
