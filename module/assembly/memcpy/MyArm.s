	.arm
	.text
	.align 2
	.globl arm_strcpy
arm_strcpy:
loop:
	LDRB R4, [R0], #1
	CMP R4, #0
	BEQ over
	STRB R4, [R1], #1
	B loop
over:
	.end
