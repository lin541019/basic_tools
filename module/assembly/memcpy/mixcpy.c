#include <stdio.h>

int main(int argc, char **argv)
{
    int a[4] = {1, 2, 3, 4};
    int b;

    __asm__ (
        "MOV R1, %1;"
	"LDMIA R1!, {R2-R5};"
	"ADD R2, R2, R3;"
	"ADD R4, R4, R5;"
	"ADD R2, R2, R4;"
	"MOV %0, R2;"
	:"=r"(b)
	:"r"(a)
    );

    printf("b = %d\n", b);

    int c = 100, d = 200;
    int result;
    __asm__ __volatile__ (
	"mov %0, %3\n\t"		    
	"ldr r1, %2\n\t"
	"str r0, %2\n\t"
	"str r1, %1\n\t"
	:"=r"(result), "+m"(c), "+m"(d)
	:"i"(123)
    );
    printf("c=%d, d=%d, result=%d\n", c, d, result);

    int e[4][2] = {{1, 2}, {3, 4}, {5, 6}, {7, 8}};
    int f;
    int *g;
    int h;

    g = &e[3][1];
    printf("g=%p, %p\n", g, &e[2][1]);

    __asm__ __volatile__
    (
	 "MOV R0, %2;"
	 "MOV R3, #0;"
	 "MOV R4, #0;"
	 "MOV R5, %3;"
	 "LOOP:;"
	     "LDMIA R0!, {R1-R2};"
	     "ADD R1, R1, R2;"
	     "ADD R3, R3, R1;" // sum = sum + R1
	     "ADD R4, R4, #1;"
	     "CMP R0, R5;"
	     "BLE LOOP\n"
	     "MOV %0, R3;"
	     "MOV %1, R5;"
	     :"=r"(f), "=r"(h)
	     :"r"(e), "r"(g)
    );

    printf("f = %d, h = %p\n", f, h);
    return 0;
}
