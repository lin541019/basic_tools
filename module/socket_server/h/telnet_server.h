#ifndef __TELNET_SERVER_H_

#define __TELNET_SERVER_H_

#include "../../../include/h/common_lib.h"
#include "../../../include/h/cus_socket.h"

#define SERV_PORT      5001
#define SERV_IPADDR    "10.0.2.4"
#define QUIT_STR       "quit"

#endif

void my_itoa(long l, char *string);
void sig_deal(int signum);

