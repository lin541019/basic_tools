#include "../../../include/h/common_lib.h"
#include "../../../include/h/cus_socket.h"

#ifndef _DEBUG_COLOR_
#define _DEBUG_COLOR_
    #define KDRK       "\x1B[0;30m"
    #define KGRY       "\x1B[1;30m"
    #define KRED       "\x1B[0;31m"
    #define KRED_L     "\x1B[1;31m"
    #define KGRN       "\x1B[1;32m"
    #define KGRN_L     "\x1B[1;32m"
    #define KYEL       "\x1B[0;33m"
    #define KYEL_L     "\x1B[1;33m"
    #define KBLU       "\x1B[0;34m"
    #define KBLU_L     "\x1B[1;34m"
    #define KMAG       "\x1B[0;35m"
    #define KMAG_L     "\x1B[1;35m"
    #define KCYN       "\x1B[0;36m"
    #define KCYN_L     "\x1B[1;36m"
    #define WHITE      "\x1B[0;37m"
    #define WHITE_L    "\x1B[1;37m"
    #define RESET      "\x1N[0m"

#endif
