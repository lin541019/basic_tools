#include <netdb.h>
#include <pthread.h>
#include "../../../include/h/cus_socket.h"
#include "../../../include/h/common_lib.h"

const int MAX_LINE = 2048;
const int PORT = 6001;
const int BACKLOG = 10;
const int LISTENQ = 6666;
const int MAX_CONNECT = 20;

