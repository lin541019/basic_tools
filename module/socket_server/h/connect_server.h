#ifndef __CONNECT_SERVER_H__

#define __CONNECT_SERVER_H__

#include "../../../include/h/common_lib.h"
#include "../../../include/h/cus_socket.h"

#include <netinet/tcp.h>

#define SERV_PORT       5001
#define SERV_IPADDR     "10.0.2.4"
#define QUIT_STR        "quit"

#endif
