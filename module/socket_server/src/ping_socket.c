#include "../h/ping_socket.h"

/* checksum processing */
unsigned short checksum(unsigned short *buf, int bufsz)
{
    unsigned long sum = 0xffff;
    while(bufsz > 1)
    {
        sum += *buf;
        buf++;
        bufsz -= 2;
    }

    if(bufsz == 1)
    {
        sum += *(unsigned char*)buf;
    }

    sum = (sum & 0xffff) + (sum >> 16);
    sum = (sum & 0xffff) + (sum >> 16);

    return ~sum;
}

int main(int argc, char *argv[])
{
    int sd;
    struct icmphdr hdr;
    struct sockaddr_in addr;
    int num;
    char buf[1024];
    struct icmphdr *icmphdrptr;
    struct iphdr *iphdrptr;
    
    if(argc!=2)
    {
        printf("usage: %s IPARRD\n", argv[0]);
        exit(-1);
    }

    addr.sin_family = PF_INET;
    
    num = inet_pton(PF_INET, argv[1], &addr.sin_addr);
    if(num < 0)
    {
        perror("inet_pton");
        exit(-1);
    }

    sd = socket(PF_INET, SOCK_RAW, IPPROTO_ICMP);
    if(sd < 0)
    {
        perror("socket");
        exit(-1);
    }

    memset(&hdr, 0, sizeof(hdr));

    hdr.type = ICMP_ECHO;
    hdr.code = 0;
    hdr.checksum = 0;
    hdr.un.echo.id = 0;
    hdr.un.echo.sequence = 0;

    hdr.checksum = checksum((unsigned short*)&hdr, sizeof(hdr));

    num = sendto(sd, (char *)&hdr, sizeof(hdr), 0, (struct sockaddr*)&addr, sizeof(addr));
    if(num<1)
    {
        perror("sendto");
        exit(-1);
    }
    
    memset(buf, 0, sizeof(buf));

    printf(KGRN"Waiting for ICMP echo ...\n");

    num = recv(sd, buf, sizeof(buf), 0);
    if(num < 1)
    {
        perror("recv");
        exit(-1);
    }

    iphdrptr = (struct iphdr*)buf;
    
    icmphdrptr = (struct icmphdr*)(buf+(iphdrptr->ihl)*4);

    switch(icmphdrptr->type)
    {
        case 3:
            printf(KBLU"The host %s is a unreachable purpose!\n", argv[1]);
            printf(KBLU"The ICMP type is %d\n", icmphdrptr->type);
            printf(KBLU"The ICMP code is %d\n", icmphdrptr->code);
            break;
        case 8:
            printf(KRED"The host %s is alive!\n", argv[1]);
            printf(KRED"The ICMP type is %d\n", icmphdrptr->type);
            printf(KRED"The ICMP code is %d\n", icmphdrptr->code);
            break;
        case 0:
            printf(KRED"The host %s is alive!\n", argv[1]);
            printf(KRED"The ICMP type is %d\n", icmphdrptr->type);
            printf(KRED"The ICMP code is %d\n", icmphdrptr->code);
            break;
        default:
            printf(KMAG"Another situations!\n");
            printf(KRED"The ICMP type is %d\n", icmphdrptr->type);
            printf(KRED"The ICMP code is %d\n", icmphdrptr->code);
            break;
 
    }
    close(sd);
    return EXIT_SUCCESS;
}
