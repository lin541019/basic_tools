#include "../h/pthread_socket.h"

/* handle recieve client function */
void *recv_message(void *fd)
{
    int sockfd = *(int *)fd;
    while(1)
    {
        char buf[MAX_LINE];
        memset(buf, 0, MAX_LINE);
        int n;
        if((n=recv(sockfd, buf, MAX_LINE, 0)) == -1)
        {
           perror("recv error.\n");
           exit(1);
        } // if
        buf[n] = '\0';
        if(strcmp(buf, "byebye.") == 0)
        {
           printf("Client closed.\n");
           close(sockfd);
           exit(1);
        } //if

        printf("\nClient: %s\n", buf);
    }// while
}

int main()
{
    // declare socket
    int listenfd, connfd;
    socklen_t clilen;
    // declare pthread_id
    pthread_t recv_tid, send_tid;
    
    // define address structure
    struct sockaddr_in servaddr, cliaddr;

    /*(1) create socket */
    if((listenfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("socket error.\n");
        exit(1);
    } //if

    /*(2) init address structure */
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);
    
    /* (3) combine socket and protocol */
    if(bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr))<0)
    {
        perror("bind error.\n");
        exit(1);
    } //if
    
    /* (4) listen */
    if(listen(listenfd, LISTENQ) < 0)
    {
        perror("listen error.\n");
        exit(1);
    }

    /* (5) apply client request, create pthread for handle */
    clilen = sizeof(cliaddr);
    if((connfd = accept(listenfd, (struct sockaddr *)&cliaddr, &clilen)) < 0)
    {
        perror("accept error.\n");
        exit(1);
    } //if

    printf("server: got connection from %s\n", inet_ntoa(cliaddr.sin_addr));

    if(pthread_create(&recv_tid, NULL, recv_message, &connfd) == -1)
    {
        perror("pthread create error.\n");
        exit(1);
    }

    /* handle server message */
    char msg[MAX_LINE];
    memset(msg, 0, MAX_LINE);
    while(fgets(msg, MAX_LINE, stdin) != NULL)
    {
        if(strcmp(msg, "exit\n") == 0)
        {
            printf("byebye.\n");
            memset(msg, 0, MAX_LINE);
            strcpy(msg, "byebye. ");
            send(connfd, msg, strlen(msg), 0);
            close(connfd);
            exit(0);
        } //if

        if(send(connfd, msg, strlen(msg), 0) == -1)
        {
            perror("send error.\n");
            exit(1);
        } //if
    } //while
}
