#include "../h/telnet_server.h"

/* this program is for telnet socket function test */
void my_itoa(long i, char *string)
{
    int power = 0, j = 0;
    j = i;

    for(power=1;j>10;j/=10)
        power *= 10;

    for(; power>0;power/=10)
    {
        *string++ = '0' + i / power;
        i %= power;
    }

    *string = '\0';
    printf("%s\n", string);
}

int server_local_fd, new_client_fd;

void sig_deal(int signum)
{
    close(new_client_fd);
    close(server_local_fd);
    exit(1);
}

int main(void)
{
    struct sockaddr_in sin;
    signal(SIGINT, sig_deal);
    printf("pid = %d\n", getpid());

    /* 1. create ipv4 tcp socket */
    server_local_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(server_local_fd < 0)
    {
        perror("socket error!");
        exit(1);
    }

    /* 2. bind server ip & port */
    bzero(&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(SERV_PORT);

#if 1
    sin.sin_addr.s_addr = inet_addr(SERV_IPADDR);
#endif

#if 0
    sin.sin_addr_s_addr = INADDR_ANY;
#endif
    /* 2.2 bind */
    if(bind(server_local_fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
        perror("bind");
        exit(1);
    }

    /* 3. listen */
    listen(server_local_fd, 5);
    printf("client listen 5. \n");

    char sned_buf[] = "hello, i am ,server\n";
    struct sockaddr_in clientaddr;
    socklen_t clientaddrlen;
    
    while(1)
    {
        /* 4. accept block client connection request */
        #if 0
            if((new_client_fd = accept(server_local_fd, NULL, NULL) < 0))
            {
                
            }
            else
            {   /* 5. communite with client (read, write) */
                ssize_t write_done = write(new_client_fd, sned_fd, sned_buf, sizeof(sned_buf));
                printf("write %ld bytes done \n", write_done);
            }
        #else
            /* get the message from client */
            memset(&clientaddr, 0, sizeof(clientaddr));
            memset(&clientaddrlen, 0, sizeof(clientaddrlen));
 
            clientaddrlen = sizeof(clientaddr);

            if((new_client_fd = accept(server_local_fd, (struct sockaddr *)&clientaddr, &clientaddrlen)) < 0)
            {
                perror("accept");
                exit(1);
            }

            printf("client connected! print the client info ... \n");
            int port = ntohs(clientaddr.sin_port);
            char ip[16] = {0};
            inet_ntop(AF_INET, &(clientaddr.sin_addr.s_addr), ip, sizeof(ip));
            printf("client: ip=%s, port=%d\n", ip, port);
        #endif
    }

    close(new_client_fd);
    close(server_local_fd);

    return 0;
}
