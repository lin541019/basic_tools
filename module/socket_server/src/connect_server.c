#include "../h/connect_server.h"

int server_local_fd, new_client_fd;

void sig_deal(int signum)
{
    close(new_client_fd);
    close(server_local_fd);
    exit(1);
}

int main(void)
{
    struct sockaddr_in sin;
    signal(SIGINT, sig_deal);
    printf("pid = %d \n", getpid());

    /* 1. create ipv4 tcp socket */
    server_local_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(server_local_fd < 0)
    {
        perror("socket error!");
        exit(1);
    }

    /* 2. bind server ip address and port */
    /* 2.1 fill struct sockaddr_in structure */
    bzero(&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_port = htons(SERV_PORT);

#if 0
    sin.sin_addr.s_addr = inet_addr(SERV_IPADDR);
#endif

#if 0
    sin.sin_addr_s_addr = INADDR_ANY;
#endif
    
#if 1
    if(inet_pton(AF_INET, "10.0.2.4", &sin.sin_addr.s_addr) > 0)
    {
        char buf[16] = {0};
        printf("s_addr=%s \n", inet_ntop(AF_INET, &sin.sin_addr.s_addr, buf, sizeof(buf)));
        printf("buf = %s \n", buf);
    }
#endif

    /* 2.2 bind */
    if(bind(server_local_fd, (struct sockaddr *)&sin, sizeof(sin)) < 0)
    {
         perror("bind");
         exit(1);
    }

    /* 3. listen */
    listen(server_local_fd, 5);
    
    printf("client listen 5. \n");

    char sned_buf[] = "hello, i am server \n";

    struct sockaddr_in clientaddr;
    socklen_t clientaddrlen;

    /* 4. accept block client connection request */
    #if 0
        /* we dont need to care about the connected information */
        if((new_client_fd = accept(server_local_fd, NULL, NULL)) < 0)
        {

        }
        else
        {
            ssize_t write_done = write(new_client_fd, sned_fd, sizeof(sned_buf));
            printf("write %ld bytes done \n", write_done);
        }
    #else
        memset(&clientaddr, 0, sizeof(clientaddr));
        memset(&clientaddrlen, 0, sizeof(clientaddrlen));

        clientaddrlen = sizeof(clientaddr);

        if((new_client_fd = accept(server_local_fd, (struct sockaddr*)&clientaddr, &clientaddrlen)) <0)
        {
            perror("accept");
            exit(1);
        }

        printf("client connected! print the client info .... \n");
        int port = ntohs(clientaddr.sin_port);
        char ip[16] = {0};
        inet_ntop(AF_INET, &(clientaddr.sin_addr.s_addr), ip, sizeof(ip));
        printf("client: ip=%s, port=%d \n", ip, port);
    #endif
    char client_buf[100] = {0};
    
#if 1
    while(1)
    {
        printf("server goes to read ... \n");
        int bytes_read_done = read(new_client_fd, client_buf, sizeof(client_buf));
        printf("bytes_read_done = %d \n", bytes_read_done);
        usleep(500000);
    }

    printf("server process end... \n");
    close(new_client_fd);
    close(server_local_fd);
#endif
    
#if 0 // case 2 : when server close a connection, system will send a SIGPIPE signal to client that the connection was invlid
// SIGPIPE signal default action is terminate(pause, exit), so client must be exit.
    while(1)
    {
        printf("server goes to read ... \n");
        int bytes_read_done = read(new_client_fd, client_buf, sizeof(client_buf));
        printf("bytes_read_done = %d \n", bytes_read_done);
        close(new_client_fd);
        while(1);
    }

    printf("server process end ... \n");
    close(server_local_fd);
#endif

#if 0 // case 3 : read() when return value < 0, it will disconnect the socket
    if(signal(SIGPIPE, SIG_DFL) == SIG_ERR)
    {
         perror("signal error");
    }
    
    char sendbuf[1024] = "hello i am server";
    
    while(1)
    {
        printf("server goes to read ... \n");
        int bytes_read_done = read(new_client_fd, client_buf, sizeof(client_buf));
        printf("bytes_read_done = %d \n", bytes_read_done);
        if(bytes_read_done <= 0)
        {
            if(errno == ENTER)
            {
                printf("network may be ok \n");
            }
            else
            {
                printf("network is not alive \n");
            }
        }

        int bytes = read(new_client_fd, client_buf, sizeof(client_buf));
        if(bytes <= 0)
        {
            if(errno == ENTER)
            {
                printf("network may be ok ... \n");
            }
            else
            {
                printf("network is not alive ... \n");
            }
        }
        
        int bytes_send_done = send(new_client_fd, sendbuf, strlen(sendbuf), 0);
        printf("bytes_send_done = %d \n", bytes_send_done);
        while(1)
        {
            printf("server is IDLE ... \n");
            usleep(500000);
        }
    }

    close(new_client_fd);
    close(server_local_fd);
#endif

#if 0
    while(1)
    {
        sleep(10);
        struct tcp_info info;
        int len = sizeof(info);
        getsockopt(new_client_fd, IPPROTO_TCP, TCP_INFO, &info, (socklen_t *)&len);

        if((info.tcpi_state == TCP_ESTABLISHED))
        {
            printf("client is connected !\n");
        }
        else
        {
            printf("client is disconnected !\n");
        }

        while(1)
        {
            printf("server is TDLE ... \n");
            usleep(500000) ;
        }

        close(new_client_fd);
        close(server_local_fd);
    }
#endif

    return 0;
}
