#include "../h/bookstore.h"

Book::Book(char *name) : index(-1), on_shelf(true)
{
    cout << "Constructor is called !" << endl;
    set_name(name);
}

Book::~Book()
{
    cout << "Destructor is called!!" << endl;
}

void Book::show_data()
{
    cout << "name :" << name;
    cout << " index : " << index;
    if(on_shelf == true)
        cout << " On shelf" << endl;
    else
        cout << " Not on shelf" << endl;
}

bool Book::lend_out()
{
    if( on_shelf == true)
    {
        on_shelf = true;
        return true;
    }
    else
        return false;
}

void Book::set_name(char *i_name)
{
    strcpy(name, i_name);
}

const char* Book::get_name()
{
    return name;
}

int main()
{
    char name[30] = "The C++ Bible";
    Book A_Book(name);
    
    cout << "Before lend out..." << endl;
    A_Book.show_data();
    A_Book.lend_out();
    cout << "After lend out..." << endl;
    A_Book.show_data();
    
    const char *getBookName = A_Book.get_name();
    cout << "Get A Book name : " << getBookName << endl;

    char B_name[30] = "Code Complete";
    Book *B_book = new Book(B_name);
    B_book->show_data();
    B_book->lend_out();

    delete B_book;

    /* declare array int */    
    int *p;
    p = (int*)malloc(sizeof(p));
    cout << sizeof(*p) << endl;
    free(p);

    return 0;
}
