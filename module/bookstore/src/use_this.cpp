#include "../h/use_this.h"

void this_ptr::show_index()
{
    cout << "idnex: " << index << endl;
}

this_ptr * this_ptr::set_index(long i_index)
{
    index = i_index;
    return this;
}

int main()
{
    this_ptr object, *object_ptr;
    object_ptr = object.set_index(1);

    cout << "object address : " << &object << endl;

    cout << "pointer address : " << object_ptr << endl;

    object.set_index(2)->show_index();

    return 0;
}
