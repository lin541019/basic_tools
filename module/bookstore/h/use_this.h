#ifndef __USE_THIS_H__
#define __USE_THIS_H__

#include <iostream>

using namespace std;

class this_ptr
{
    private:
        long index;
    public:
        void show_index();
        this_ptr * set_index(long i_index);
};

#endif
