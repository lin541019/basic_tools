#ifndef __BOOKSTORE_H__
#define __BOOKSTORE_H__

#include <iostream>
#include <cstring>

using namespace std;

class Book
{
    private:
        char name[30];
        long index;
        bool on_shelf;
    public:
        Book(char *name);
        ~Book();

void set_name(char *i_name);
void show_data();
const char* get_name();
bool lend_out();

};

class Reader
{
    private:
        const char name[10];
        static int id;
    public:
        const char* get_name() const
        {
            return name;
        }
};



#endif
