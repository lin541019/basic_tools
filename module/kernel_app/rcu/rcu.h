#ifndef __RCU__
#define __RCU__

#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/kthread.h>
#include <linux/rcupdate.h>
#include <linux/delay.h>

struct foo
{
    int a;
    int b;
    int c;
    struct rcu_head rcu;
    struct list_head list;
};

struct foo *g_pfoo = NULL;

struct task_struct *rcu_reader_t;
struct task_struct *rcu_updater_t;
struct task_struct *rcu_reader_list_t;
struct task_struct *rcu_updater_list_t;
#endif /* define __RCU__ */

