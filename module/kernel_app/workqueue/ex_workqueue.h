#ifndef __EX_WORKQUEUE_H__
#define __EX_WORKQUEUE_H__

#include <linux/init.h>
#include <linux/module.h>
#include <linux/workqueue.h>
#include <linux/slab.h>

struct workqueue_struct *wq;

struct work_data 
{
    struct work_struct my_work;
    int the_data;
};

#endif /* #define __EX_WORKQUEUE_H__ */
