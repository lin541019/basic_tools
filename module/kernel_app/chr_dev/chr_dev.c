#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/device.h>

/* set the device name */
#define     DEV_NAME         "charDev"
#define     DEV_CNT          (3)
/* set prefix device node */
#define     DEV_INODE_NAME   "chrdev" 
#define     DEV_CLASS_NAME   "charDev_class"

#define     BUFF_SIZE        128

/* chrdev id. secondary device id */
typedef enum
{
    eChrDev_1 = 0,
    eChrDev_2,
}eChrDev_ID;

/* variable */
static dev_t devNumStar;
struct class *chrdev_class;

/* create device management structure */
struct chr_dev
{
    unsigned char ID;
    dev_t devNum;
    struct cdev dev;

    char cBuf[BUFF_SIZE];
    unsigned int rIndex;
    unsigned int wIndex;

};

static struct chr_dev CHR_DEV[DEV_CNT];

/* 1. 1.2 implement driver procedure */

/** @brief chrdev_open
  * @param
  * @retval
  * @author lzm
  */
int chrdev_open(struct inode *inode, struct file *filp)
{
    printk("chrdev_open!\n");
    printk("device num is [%d]\n", inode->i_rdev);

    return 0;
}

/** @brief chrdev_release
  * @param
  * @retval
  * @author lzm
  */
int chrdev_release(struct inode *inode, struct file *filp)
{
    printk("chrdev_release!\n");
    printk("device num is [%d]\n", inode->i_rdev);
    return 0;
}

/** @brief chrdev_write
  * @param
  * @retval
  * @author lzm
  */
ssize_t chrdev_write(struct file *filp, const char __user * buf, size_t count, loff_t *ppos)
{
    int ret;
    int tmp = count;
    unsigned char curMinDevNum = MINOR(filp->f_inode->i_rdev);

    printk("write enter pass\n");

    if(curMinDevNum >= DEV_CNT)
        return 0;

    printk("write min device num is [%d]!\n", curMinDevNum);
    /* wrtie the data always start from device number = 0 */
    CHR_DEV[curMinDevNum].wIndex = 0;

    if(tmp>BUFF_SIZE-CHR_DEV[curMinDevNum].wIndex)
        tmp = BUFF_SIZE - CHR_DEV[curMinDevNum].wIndex;

    ret = copy_from_user(CHR_DEV[curMinDevNum].cBuf, buf, tmp);
    CHR_DEV[curMinDevNum].wIndex += tmp;

    return count;
}

/** @brief chrdev_read
  * @param
  * @retval
  * @author lzm
  */
ssize_t chrdev_read(struct file *filp, char __user * buf, size_t count, loff_t *ppos)
{
    int ret;
    //unsigned long p = *pos; // internal save, it is independent with *ppos in write function */
    int tmp = count;
    unsigned char curMinDevNum = MINOR(filp->f_inode->i_rdev);

    printk("read min device num is [%d]!\n", curMinDevNum);
    printk("read wIndex is [%d]", CHR_DEV[curMinDevNum].wIndex);

    // read only the written data
    if(tmp > CHR_DEV[curMinDevNum].wIndex)
        tmp = CHR_DEV[curMinDevNum].wIndex;

    ret = copy_to_user(buf, CHR_DEV[curMinDevNum].cBuf, tmp);
    CHR_DEV[curMinDevNum].rIndex += tmp;

    return tmp;
}

/*1. 1.1 fill into fire operation */
static struct file_operations chr_dev_fops = 
{
    .owner         = THIS_MODULE,
    .open          = chrdev_open,
    .release       = chrdev_release,
    .write         = chrdev_write,
    .read          = chrdev_read,
};

/* No.1 Module Init */
/** @brief chrdev_init
  * @param
  * @retval
  * @author lzm
  */
static int __init chrdev_init(void)
{
    int ret = 0;
    unsigned char i;
    printk("chrdev_init\n") ;

    /* 1. get device num */
    ret = alloc_chrdev_region(&devNumStar, 0, DEV_CNT, DEV_NAME);
    if(ret < 0)
    {
        printk("chrdev_init error for not get devNum!\n");
        return -1;
    }
    else
    {
        printk("get devNumStar[%d]\n", devNumStar);
    }

    /* 2. create device class */
    chrdev_class = class_create(THIS_MODULE, DEV_CLASS_NAME);
    
    for(i=0;i<DEV_CNT;i++)
    {
        /* get ID and devNum */
        CHR_DEV[i].ID = i;
        CHR_DEV[i].devNum = MKDEV(MAJOR(devNumStar), i);
        CHR_DEV[i].rIndex = 0;
        CHR_DEV[i].wIndex = 0;

        /* 3. init cdev and bind file_operation */
        cdev_init(&CHR_DEV[i].dev, &chr_dev_fops);

        /* 4. register cdev for kernel */
        ret = cdev_add(&CHR_DEV[i].dev, CHR_DEV[i].devNum, 1);

        if(ret < 0)
        { 
            printk("[%d] cdev_add error!\n", i);
            unregister_chrdev_region(CHR_DEV[i].devNum, 1);
            return -1;
        }
        else
        {
            printk("[%d] cdev_add successful!\n", i);
        }

        /* 5. create device inode */
        device_create(chrdev_class, NULL, CHR_DEV[i].devNum, NULL, DEV_INODE_NAME "%d", i);
    }
    return 0;
}
module_init(chrdev_init);

/* No.2 Module Exit! */
/** @brief chrdev_exit
  * @param
  * @retval
  * @author lzm
  */
static void __exit chrdev_exit(void)
{
    unsigned char i;
    printk("chrdev_exit!\n");

    for(i=0;i<DEV_CNT;i++)
    {
        device_destroy(chrdev_class, CHR_DEV[i].devNum); // 1. delete device node
        cdev_del(&CHR_DEV[i].dev);                       // 2. delete char device in kernel
    }

    unregister_chrdev_region(devNumStar, DEV_CNT);       // 3. release device number
    class_destroy(chrdev_class);                         // 4. delete device class

}
module_exit(chrdev_exit);

/* NO.3 LICENSE and info */
MODULE_AUTHOR("lintzuming");
MODULE_DESCRIPTION("hello world module");
MODULE_ALIAS("test module");
MODULE_LICENSE("GPL");
