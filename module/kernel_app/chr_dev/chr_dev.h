#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

/* set the device name */
#define     DEV_NAME         "charDev"
#define     DEV_CNT          (3)
/* set prefix device node */
#define     DEV_INODE_NAME   "chrdev" 
#define     DEV_CLASS_NAME   "charDev_class"

#define     BUFF_SIZE        128

/* chrdev id. secondary device id */
typedef enum
{
    eChrDev_1 = 0,
    eChrDev_2,
}eChrDev_ID;

/* variable */
static dev_t devNumStar;
static class *chrdev_class;

/* create device management structure */
struct chr_dev
{
    unsigned char ID;
    dev_t devNum;
    struct cdev dev;

    char cBuf[BUFF_SIZE];
    unsigned int rIndex;
    unsigned int wIndex;

};

static struct chr_dev CHR_DEV[DEV_CNT];
