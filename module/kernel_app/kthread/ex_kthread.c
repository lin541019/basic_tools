#include "ex_kthread.h"

MODULE_LICENSE("GPL");

static struct task_struct *brook_tsk;
static int data;

static int kbrook(void *arg)
{
    unsigned int timeout;
    int *d = (int *)arg;

    for(;;)
    {
        if(kthread_should_stop()) break;
        printk("%s(): %d\n", __FUNCTION__, (*d)++);
        do
        {
            set_current_state(TASK_INTERRUPTIBLE);
            timeout = schedule_timeout(10 * HZ);
        }while(timeout);
    }
    printk("break\n");
    return 0;
}

static int __init init_modules(void)
{
    int ret;
    
    brook_tsk = kthread_create(kbrook, &data, "brook");
    if(IS_ERR(brook_tsk))
    {
        ret = PTR_ERR(brook_tsk);
        brook_tsk = NULL;
        goto out;
    }
    wake_up_process(brook_tsk);
    return 0;

out:
    return ret;
}

static void __exit exit_modules(void)
{
    kthread_stop(brook_tsk);
}

module_init(init_modules);
module_exit(exit_modules);
