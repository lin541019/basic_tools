#include "pltfrm_dev.h"

struct platform_device demo_device =
{
    .name = DEVNAME,
};

static int demo_probe(struct platform_device *pdev)
{
    pr_info("%s(#%d)\n", __func__, __LINE__);
    return 0;
}

static int demo_remove(struct platform_device *pdev)
{
    pr_info("%s(#%d)\n", __func__, __LINE__);
    return 0;
}

struct platform_driver demo_driver =
{
    .driver = {
        .name = DEVNAME,
        .owner = THIS_MODULE,
    },
    .probe = demo_probe,
    .remove = demo_remove,
};

MODULE_AUTHOR("LZM");
MODULE_DESCRIPTION("Kernel module for demo");
MODULE_LICENSE("GPL");

static int __init demo_init(void)
{
    int err;
    pr_info("%s(#%d)\n", __func__, __LINE__);
    
    err = platform_device_register(&demo_device);
    if(err)
    {
        pr_err("%s(#%d): platform_device_register failed(%d)\n", __func__, __LINE__, err);
        return err;
    }

    err = platform_driver_register(&demo_driver);
    if(err)
    {
        dev_err(&(demo_device.dev), "%s(#%d): platform_driver_register fail(%d)\n", __func__, __LINE__, err);
        goto dev_reg_failed;
    }
    return err;

dev_reg_failed:
    platform_device_unregister(&demo_device);
    return err;
}
module_init(demo_init);

static void __exit demo_exit(void)
{
    dev_info(&(demo_device.dev), "%s(#%d)\n", __func__, __LINE__);
    platform_device_unregister(&demo_device);
    platform_driver_unregister(&demo_driver);
}
module_exit(demo_exit);
