#ifndef __GPIO_RESOURCE_H__
#define __GPIO_RESOURCE_H__

#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>

#define LED_GPIO_PIN 17
#define BUTTON_GPIO_PIN 22
#define s_BlinkPeriod 1000

#endif // #define __GPIO_RESCOUCE_H__
