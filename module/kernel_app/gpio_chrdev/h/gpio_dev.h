#ifndef __GPIO_DEV_H__
#define __GPIO_DEV_H__

#include <linux/mm.h>
#include <asm/io.h>

/*#define GPIO_BASE	0xFE000000 + 0x00200000 */
#define GPIO_BASE	0xFE200000

void InitGPIO(int GPIO);
void SetGPIOOutput(int GPIO, bool outputValue);

#endif // @define __GPIO_DEV_H__
