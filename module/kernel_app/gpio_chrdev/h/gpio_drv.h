/** @file        gpio_drv.h
 *  @brief       driver
 *  @details     led driver programmer, maily file_operations
 *  @author      lzm
 *  @date        2021-05-10 
 *  @version     v1.0
 *  @copyright   Copyright By lintzuming, All Rights Reserved
 *
 *  *******************
 *  @LOG revise work daily
 *  *******************
 */

#ifndef __gpio_drv_h__
#define __gpio_drv_h__

#include "gpio_resource.h"
#include "gpio_dev.h"

/* set the device name */
#define     DEV_NAME         "charDev"
#define     DEV_CNT          (3)
/* set prefix device node */
#define     DEV_INODE_NAME   "chrdev" 
#define     DEV_CLASS_NAME   "charDev_class"

#define     BUFF_SIZE        128

/* chrdev id. secondary device id */
typedef enum
{
    eChrDev_1 = 0,
    eChrDev_2,
}eChrDev_ID;

/* create device management structure */
struct chr_dev
{
    unsigned char ID;
    dev_t devNum;
    struct cdev dev;

    char cBuf[BUFF_SIZE];
    unsigned int rIndex;
    unsigned int wIndex;

};

static const struct gpio key = {
    .gpio = 22,
    .flags = GPIOF_IN,
    .label = "key0"
};

static struct chr_dev CHR_DEV[DEV_CNT];

int chrdev_open(struct inode *inode, struct file *filp);
int chrdev_release(struct inode *inode, struct file *filp);
ssize_t chrdev_write(struct file *filp, const char __user * buf, size_t count, loff_t *ppos);
ssize_t chrdev_read(struct file *filp, char __user * buf, size_t count, loff_t *ppos);
int strcmp(const char *s1, const char *s2);

#endif // #define __gpio_drv_h__
