/** @file      gpio_module.c
 *  @brief     driver.
 *  @details   gpio module file
 *  @author    lzm
 *  @date      2021-05-10 
 *  @version   v1.0
 *  @copyright Copyright By lintzuming, All Rights Reserved
 *
 *  ************************
 *  @LOG revise blog
 *  ************************
 */

#include "h/gpio_drv.h"
#include "h/gpio_dev.h"
#include "h/gpio_resource.h"

extern dev_t devNumStar;
extern struct class *chrdev_class;
extern void InitGPIO(int GPIO);
extern void SetGPIOOutput(int GPIO, bool outputValue);
extern volatile unsigned *gpio;
extern void timer_callback(struct timer_list *cb_timer);
extern irqreturn_t button_ih(int irq, void* dev);
struct timer_list gpio_timer;
unsigned int key_irq = 0;

static struct file_operations chr_dev_fops = 
{
    .owner         = THIS_MODULE,
    .open          = chrdev_open,
    .release       = chrdev_release,
    .write         = chrdev_write,
    .read          = chrdev_read,
};

/* No.1 Module Init */
/** @brief chrdev_init
  * @param
  * @retval
  * @author lzm
  */
static int __init chrdev_init(void)
{
    int ret = 0;
    unsigned char i;

    /* 1. get device num */
    ret = alloc_chrdev_region(&devNumStar, 0, DEV_CNT, DEV_NAME);
    if(ret < 0)
    {
        printk("chrdev_init error for not get devNum!\n");
        return -1;
    }
    else
    {
        printk("get devNumStar[%d]\n", devNumStar);
    }

    /* 2. create device class */
    chrdev_class = class_create(THIS_MODULE, DEV_CLASS_NAME);
    
    for(i=0;i<DEV_CNT;i++)
    {
        /* get ID and devNum */
        CHR_DEV[i].ID = i;
        CHR_DEV[i].devNum = MKDEV(MAJOR(devNumStar), i);
        CHR_DEV[i].rIndex = 0;
        CHR_DEV[i].wIndex = 0;

        /* 3. init cdev and bind file_operation */
        cdev_init(&CHR_DEV[i].dev, &chr_dev_fops);

        /* 4. register cdev for kernel */
        ret = cdev_add(&CHR_DEV[i].dev, CHR_DEV[i].devNum, 1);

        if(ret < 0)
        { 
            printk("[%d] cdev_add error!\n", i);
            unregister_chrdev_region(CHR_DEV[i].devNum, 1);
            return -1;
        }
        else
        {
            printk("[%d] cdev_add successful!\n", i);
        }

        /* 5. create device inode */
        device_create(chrdev_class, NULL, CHR_DEV[i].devNum, NULL, DEV_INODE_NAME "%d", i);
    }
    
    /* init gpio default setting, we take led for example here */
    gpio = (volatile unsigned *)ioremap(GPIO_BASE, sizeof(gpio));
    InitGPIO(LED_GPIO_PIN);
    /* init gpio.h setting */
    int rc = 0;
    
    if((rc = gpio_request_one(key.gpio, key.flags, key.label)) < 0)
    {
        printk(KERN_ERR "ERROR%d: cannot request gpio\n", rc);
        return rc;
    }
    
    key_irq = gpio_to_irq(key.gpio);
    if(key_irq < 0)
    {
        printk(KERN_ERR "ERROR%d:cannot get irq num\n", key_irq);
        return key_irq;
    }
    
    if(request_irq(key_irq, button_ih, IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, "button_irq", NULL) < 0)
    {
        printk(KERN_ERR "cannot request irq\n");
        return -EFAULT;
    }
    
    printk("module init, Hello GPIO!\n");
    
#if 0
    timer_setup(&gpio_timer, timer_callback, 0);

    mod_timer(&gpio_timer, jiffies + msecs_to_jiffies(s_BlinkPeriod));
    
    del_timer(&gpio_timer);
#endif
    return 0;
}
module_init(chrdev_init);

/* No.2 Module Exit! */
/** @brief chrdev_exit
  * @param
  * @retval
  * @author lzm
  */
static void __exit chrdev_exit(void)
{
    unsigned char i;

    for(i=0;i<DEV_CNT;i++)
    {
        device_destroy(chrdev_class, CHR_DEV[i].devNum); // 1. delete device node
        cdev_del(&CHR_DEV[i].dev);                       // 2. delete char device in kernel
    }

    unregister_chrdev_region(devNumStar, DEV_CNT);       // 3. release device number
    class_destroy(chrdev_class);                         // 4. delete device class
    
    iounmap(gpio);

	del_timer(&gpio_timer);
	printk("GPIO Module : Goodbye!\n");

}
module_exit(chrdev_exit);

/* NO.3 LICENSE and info */
MODULE_AUTHOR("lintzuming");
MODULE_DESCRIPTION("gpio char device module");
MODULE_ALIAS("test module");
MODULE_LICENSE("GPL");
