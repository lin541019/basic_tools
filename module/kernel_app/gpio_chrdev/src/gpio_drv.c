#include "../h/gpio_drv.h"

/* variable */
dev_t devNumStar;
struct class *chrdev_class;

extern struct timer_list gpio_timer;
extern void SetGPIOOutput(int GPIO, bool outputValue);
extern void timer_callback(struct timer_list *cb_timer);

char *state_on = "on";
char *state_off = "off";
char *state_timer = "timer";
int timer_start = 0;

/* interrupt key press handler */
irqreturn_t button_ih(int irq, void* dev)
{
    int state = gpio_get_value(BUTTON_GPIO_PIN);
    if(state)
    {
        SetGPIOOutput(LED_GPIO_PIN, true);    
    }
    else
    {
        SetGPIOOutput(LED_GPIO_PIN, false);    
    }
    //printk(KERN_INFO "key pressed\n");
    
    return IRQ_HANDLED;
}
/*main function*/
void timer_callback(struct timer_list *cb_timer){
    static bool on = false;
    on = !on;
    SetGPIOOutput(LED_GPIO_PIN, on);
    mod_timer(cb_timer, jiffies + msecs_to_jiffies(s_BlinkPeriod));
    //printk("jiffies = %ld\n", jiffies+msecs_to_jiffies(s_BlinkPeriod));
}

int strcmp(const char *s1, const char *s2)
{
    while(*s1 && (*s1==*s2))
    {
        s1++;
        s2++;
    }
    return *(const unsigned char*)s1-*(const unsigned char*)s2;
}

/** @brief chrdev_open
  * @param
  * @retval
  * @author lzm
  */
int chrdev_open(struct inode *inode, struct file *filp)
{
    printk("chrdev_open!\n");
    printk("device num is [%d]\n", inode->i_rdev);

    return 0;
}

/** @brief chrdev_release
  * @param
  * @retval
  * @author lzm
  */
int chrdev_release(struct inode *inode, struct file *filp)
{
    printk("chrdev_release!\n");
    printk("device num is [%d]\n", inode->i_rdev);
    return 0;
}

/** @brief chrdev_write
  * @param
  * @retval
  * @author lzm
  */
ssize_t chrdev_write(struct file *filp, const char __user * buf, size_t count, loff_t *ppos)
{
    int ret;
    int tmp = count;
    unsigned char curMinDevNum = MINOR(filp->f_inode->i_rdev);

    if(curMinDevNum >= DEV_CNT)
        return 0;

    printk("write min device num is [%d]!\n", curMinDevNum);
    /* wrtie the data always start from device number = 0 */
    CHR_DEV[curMinDevNum].wIndex = 0;

    if(tmp>BUFF_SIZE-CHR_DEV[curMinDevNum].wIndex)
        tmp = BUFF_SIZE - CHR_DEV[curMinDevNum].wIndex;

    ret = copy_from_user(CHR_DEV[curMinDevNum].cBuf, buf, tmp);
    CHR_DEV[curMinDevNum].wIndex += tmp;
    
    if(strcmp(buf, state_on) == 0)
    {
        if(timer_start)
            del_timer(&gpio_timer);
        printk("set state on\n");
        SetGPIOOutput(LED_GPIO_PIN, true);
    }
    else if(strcmp(buf, state_off) == 0)
    {
        if(timer_start)
            del_timer(&gpio_timer);
        printk("set state off\n");
        SetGPIOOutput(LED_GPIO_PIN, false);
    }
    else if(strcmp(buf, state_timer) == 0)
    {
        printk("set timer gpio on\n");
        timer_setup(&gpio_timer, timer_callback, 0);
        mod_timer(&gpio_timer, jiffies + msecs_to_jiffies(s_BlinkPeriod));
        timer_start = 1;
    }
    else
        printk("Oops ... The command is invlaid now\n");

    return count;
}

/** @brief chrdev_read
  * @param
  * @retval
  * @author lzm
  */
ssize_t chrdev_read(struct file *filp, char __user * buf, size_t count, loff_t *ppos)
{
    int ret;
    //unsigned long p = *pos; // internal save, it is independent with *ppos in write function */
    int tmp = count;
    unsigned char curMinDevNum = MINOR(filp->f_inode->i_rdev);

    printk("read min device num is [%d]!\n", curMinDevNum);
    printk("read wIndex is [%d]", CHR_DEV[curMinDevNum].wIndex);

    // read only the written data
    if(tmp > CHR_DEV[curMinDevNum].wIndex)
        tmp = CHR_DEV[curMinDevNum].wIndex;

    ret = copy_to_user(buf, CHR_DEV[curMinDevNum].cBuf, tmp);
    CHR_DEV[curMinDevNum].rIndex += tmp;

    return tmp;
}
