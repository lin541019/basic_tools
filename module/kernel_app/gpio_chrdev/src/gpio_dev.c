#include "../h/gpio_dev.h"

struct GpioRegisters *s_pGpioRegisters;

volatile unsigned *gpio;

/*GPIO macro */
#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<((g)%10)*3)
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<((g)%10)*3)

#define GPIO_SET *(gpio+7)
#define GPIO_CLR *(gpio+10)

void InitGPIO(int GPIO_PIN)
{
    INP_GPIO(GPIO_PIN);
    OUT_GPIO(GPIO_PIN);
}

void SetGPIOOutput(int GPIO_PIN, bool outputValue)
{
    if(outputValue)
    {
        GPIO_SET = 1 << GPIO_PIN;
    }
    else
    {
        GPIO_CLR = 1 << GPIO_PIN;
    }
}
