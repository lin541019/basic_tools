#include "msc_dev.h"
#include <linux/module.h>
#include <linux/init.h>
//#include <linux/moduleparam.h>
#include <linux/types.h>
#include <linux/miscdevice.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <asm/io.h>
#include <asm/uaccess.h>

struct fellowmisc_dev{
    struct miscdevice misc;
    struct miscdata data;
};

struct fellowmisc_dev *fellowmisc_devp;

int fellowmisc_open(struct inode *inode, struct file *filep)
{
    filep->private_data = fellowmisc_devp;
    return 0;
}
int fellowmisc_release(struct inode *inode, struct file *filep)
{
    return 0;
}

long fellowmisc_ioctl(struct file *filep,unsigned int cmd,unsigned long arg)
{
    int ret = 0;
    struct fellowmisc_dev *devp = (struct fellowmisc_dev *)(filep->private_data);
    if (_IOC_TYPE(cmd) != FELLOW_MISC_IOC_MAGIC)
        return -EINVAL;
    if (_IOC_NR(cmd) > FELLOW_MISC_IOC_MAXNR)
        return -EINVAL;
    switch(cmd)
    {
        case FELLOW_MISC_IOC_PRINT:
            printk("FELLOW_MISC_IOC_PRINT\n");
            printk("val:%d, size: %d, str: %s\n", devp->data.val, devp->data.size, devp->data.str);
        break;
        case FELLOW_MISC_IOC_SET:
            printk("FELLOW_MISC_IOC_SET\n");
            ret = copy_from_user((unsigned char*)&(devp->data), (unsigned char *)arg, sizeof(struct miscdata));
            printk("set val:%d, size: %d, str: %s\n", devp->data.val, devp->data.size, devp->data.str);
        break;
        case FELLOW_MISC_IOC_GET:
            printk("FELLOW_MISC_IOC_GET\n");
            ret = copy_to_user((unsigned char*)arg,(unsigned char*)&(devp->data), sizeof(struct miscdata));
        break;
        default:
            return -EINVAL;
    }
    return ret;
}

static const struct file_operations fellowmisc_fops ={
    .owner = THIS_MODULE,
    .open = fellowmisc_open,
    .release = fellowmisc_release,
    .unlocked_ioctl = fellowmisc_ioctl,
};

static struct miscdevice fellow_misc = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "fellowplat",
    .fops = &fellowmisc_fops,
};

static struct platform_device *fellow_platform_device;
static int fellow_plat_drv_probe(struct platform_device *dev)
{
    int error;
    printk("fellow_plat_drv_probe\n");
    fellowmisc_devp = kmalloc(sizeof(struct fellowmisc_dev), GFP_KERNEL);
    if (!fellowmisc_devp)
    {
        error = -ENOMEM;
        return error;
    }
    memset(&(fellowmisc_devp->data), 0, sizeof(fellowmisc_devp->data));
    fellowmisc_devp->misc = fellow_misc;
    error = misc_register(&fellow_misc);
    return error;
}
static int fellow_plat_drv_remove(struct platform_device *dev)
{
    if (fellowmisc_devp)
        kfree(fellowmisc_devp);
    misc_deregister(&fellow_misc);
    return 0;
}
static struct platform_driver fellow_platform_driver = {
.driver = {
.name = "fellow",
},
.probe = fellow_plat_drv_probe,
.remove = fellow_plat_drv_remove,
};

static int fellowplat_init(void)
{
    int error;
    printk("fellowplat_init\n");
    printk("fellow register driver\n");
    error = platform_driver_register(&fellow_platform_driver);//注册platform driver
    if (error)
        return error;

    fellow_platform_device = platform_device_alloc("fellow", -1);//名字与platform driver相同。
    if (!fellow_platform_device) {
        error = -ENOMEM;
        goto err_driver_unregister;
    }

    printk("fellow register device\n");
    error = platform_device_add(fellow_platform_device);//添加platform device
    if (error)
        goto err_free_device;

    return 0;

err_free_device:
    platform_device_put(fellow_platform_device);
err_driver_unregister:
    platform_driver_unregister(&fellow_platform_driver);
    return error;
}

static void fellowplat_exit(void)
{
    platform_device_unregister(fellow_platform_device);
    platform_driver_unregister(&fellow_platform_driver);
}

MODULE_AUTHOR("fellow");
MODULE_LICENSE("GPL");
module_init(fellowplat_init);
module_exit(fellowplat_exit);
